package GM;


import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

import java.awt.*;

public class DontShot extends Robot {

	boolean peek;
	boolean direction = false;
	double moveAmount;

	public void run() {
		setBodyColor(Color.red);
		setGunColor(Color.black);
		setRadarColor(Color.white);
		setBulletColor(Color.red);
		setScanColor(Color.white);
 		searchEnemey();
		moveAmount = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
		peek = false;
		turnLeft(getHeading() % 90);
		ahead(moveAmount);
		peek = true;
		turnGunRight(90);
		turnRight(90);
		searchEnemey();
		//direita
		while (direction == true) {
			peek = true;
			ahead(moveAmount);
			peek = false;
			turnLeft(90);
			searchEnemey();
			
		}
		//esquerda
			while (direction == false) {
			peek = true;
			back(moveAmount);
			peek = false;
			turnLeft(90);
			searchEnemey();
			
		}
	}
	public void searchEnemey() {
	turnGunRight(90);
	turnGunLeft(180);
	turnGunRight(90);
	}
	
	public void fastSearchEnemey() {
	turnGunRight(45);
	turnGunLeft(90);
	turnGunRight(45);
	}
	
	public void onHitRobot(HitRobotEvent e) {
		if (e.getBearing() > -90 && e.getBearing() < 90) {
		if(direction == true){
			direction = false;
		}else{
			direction = true;
		}
			back(moveAmount);
		}	else {
				if(direction == true){
			direction = false;
		}else{
			direction = true;
		}
		ahead(moveAmount);
		}
	}
	public void onScannedRobot(ScannedRobotEvent e) {
		if(direction == true ){
			fire(2);
			ahead(100);
		}
		if(direction == false ){
			fire(2);
			back(100);

		}
		if (peek) {
			scan();
		}
	}
}
